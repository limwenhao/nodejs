const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const router = express.Router();

router.get('/home', (req,res) => {
    res.send('Hello World, This is home router');
});

router.get('/profile', (req,res) => {
    res.send('Hello World, This is profile router');
});

router.get('/login', (req,res) => {
    res.send('Hello World, This is login router');
});

router.get('/logout', (req,res) => {
res.send('Hello World, This is logout router');
});

// router.use((req,res,next)=>{
//     console.log('Time: ', Date.now());
//     next();
// })

// router.get('/home',(req,res)=>{
//     res.send('ok');
// })

// router.get('/',(req,res)=>{
//     res.send('ok empty');
// })

// app.use((err,req,res,next)=>{
//     res.status(500).send('Something broke');
// });

app.use(bodyParser.json());

app.use('/',router);
app.listen(process.env.port || 3000);

console.log('Web server running at port ' + (process.env.port || 3000));