const mysql = require("mysql");
const express = require('express');
const app = express();
const router = express.Router();

const pool = mysql.createPool({
    connectionLimit : 10,
    host : "localhost",
    user : "root",
    password : "root",
    database : "testDB",
    port : "3306",
    debug : "false"
});

router.get('/', (req,res)=>{
    pool.query("Select * from customers", (err,rows) => {
        if(err){
            console.log("error occur during connection");
        };
        var data = rows.map(v => Object.assign({},v));
        res.send(data);
    })
})

app.use('/', router);

app.listen(process.env.port || 3000);

console.log('Web Server is listening at port '+ (process.env.port || 3000));
